import glob
import os
import subprocess


def mk_dir_model(L1=None, L2=None, dir_output=None):
    part_L1 = "__L1_{}".format(L1) if L1 else ""
    part_L2 = "__L2_{}".format(L2) if L2 else ""
    fname_model = "model{}{}".format(part_L1, part_L2)
    return os.path.join(dir_output, fname_model) if dir_output else fname_model


def mk_cmd_vw_train(path_model, path_readable_model, path_data=None, L1=None, L2=None):
    cmd = " ".join([
        "vw",
        "--cache",
        "--holdout_off",
        "--normalized",
        "--loss_function squared",
        "--passes 50",
        "--data {}".format(path_data) if path_data and path_data != "-" else "",
        "--final_regressor {}".format(path_model),
        "--readable_model {}".format(path_readable_model),
        "--l1 {}".format(L1) if L1 else "",
        "--l2 {}".format(L2) if L2 else "",
    ])
    return ("rm {}.cache; ".format(path_data) if path_data else "") + cmd


def mk_cmd_vw_predict(path_invert_hash, path_initial_regressor, path_predictions, path_data=None):
    cmd = " ".join([
        "vw",
        "--kill_cache",
        "--invert_hash {}".format(path_invert_hash),
        "--testonly",
        "--data {}".format(path_data) if path_data and path_data != "-" else "",
        "--initial_regressor {}".format(path_initial_regressor),
        "--predictions {}".format(path_predictions),
    ])
    return cmd


def mk_cmd_vw_paste(path_predictions, path_data, path_combined):
    cmd = " ".join([
        "! paste -d",
        "' '",
        "{}".format(path_predictions) if path_predictions else "",
        "{}".format(path_data) if path_data else "",
        ">",
        "{}".format(path_combined) if path_combined else ""
    ])
    return cmd


def mk_cmd_vw_metrics(path_combined, path_metrics):
    cmd = " ".join([
        "! mpipe metrics-reg",
        "--has-r2",
        "{}".format(path_combined) if path_combined else "",
        "-o",
        "{}".format(path_metrics) if path_metrics else ""
    ])
    return cmd





dir_output = os.path.join("014/assignment_2/output")

os.system("mkdir -p {}".format(dir_output))
dir_models = os.path.join(dir_output,
                          "models")

L1s = [1e-2, 1e-4, 1e-6, 1e-7, 1e-8]

path_training = os.path.join("014/assignment_2/train.vw")
path_validation = os.path.join("014/assignment_2/test.vw")

FILENAME_METRICS = "metrics.txt"
FILENAME_VW_REGRESSOR = "regressor.vw_model"
FILENAME_READABLE_MODEL = "readable_model.txt"
FILENAME_PREDICTIONS = "predicted.txt"
FILENAME_Inverted_hash = "hash.invert_hash"
FILENAME_vector_for_metrics = "vectors_for_metrics.txt"

for L1 in L1s:
    dir_model = mk_dir_model(L1, dir_output=dir_models)
    os.makedirs(dir_model, exist_ok=True)

    cmd_train = mk_cmd_vw_train(
        path_model=os.path.join(
            dir_model,
            FILENAME_VW_REGRESSOR
        ),
        path_readable_model=os.path.join(
            dir_model,
            FILENAME_READABLE_MODEL),
        path_data=path_training,
        L1=L1)
    print(f"Apmokomas modelis su L1={L1} reguliarizacijos reiksme")
    subprocess.run(cmd_train, shell=True)
    #predictions
    cmd_predict = mk_cmd_vw_predict(
        path_initial_regressor=os.path.join(
            dir_model,
            FILENAME_VW_REGRESSOR
        ),
        path_predictions=os.path.join(
            dir_model,
            FILENAME_PREDICTIONS),
        path_invert_hash=os.path.join(
            dir_model,
            FILENAME_Inverted_hash),
        path_data=path_validation
    )
    subprocess.run(cmd_predict, shell=True)
    print(f"Buvo baigtas apmokyti ir tikrinti modelis su L1 ={L1}")
    # metrics
    cmd_paste = mk_cmd_vw_paste(
        path_predictions=os.path.join(
            dir_model,
            FILENAME_PREDICTIONS),
        path_data=path_validation,
        path_combined=os.path.join(
            dir_model,
            FILENAME_vector_for_metrics))
    subprocess.run(cmd_paste, shell=True)
    cmd_metrics = mk_cmd_vw_metrics(
        path_combined=os.path.join(
            dir_model,
            FILENAME_vector_for_metrics),
        path_metrics=os.path.join(
            dir_model,
            FILENAME_METRICS))
    subprocess.run(cmd_metrics, shell=True)
    metric_paths = glob.glob("014/assignment_2/output/models/*/metrics.txt")
    metrics_combined = os.path.join(
        dir_output,
        "metrics_combined.txt"
    )
    os.system("ls 014/assignment_2/output/models/*/metrics.txt | sort | mungy catcsv > {}".format(metrics_combined))